1. Create a directory in your home: 
    $ ~/bin/dotfiles/vim
2. Clone from bitbucket: 
    $ git clone git@bitbucket.org:timhiggins/Vim.git ~/bin/dotfiles/vim
3. Clone Vundle into the Vim directory structure:
    $ git clone https://github.com/VundleVim/Vundle.vim.git ~/bin/dotfiles/vim/bundle/Vundle.vim
4. Update plugins: 
    $ vim +PluginInstall +qall
5. From the home directory, create the necessary symbolic links:
    $ ln -nfs ~/bin/dotfiles/vim/vimrc .vimrc
    $ ln -nfs ~/bin/dotfiles/vim .vim
    $ ln -nfs ~/bin/dotfiles/vim/vrapperrc .vrapperrc

